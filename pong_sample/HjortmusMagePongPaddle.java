package pong_sample;

import javafx.scene.shape.Rectangle;

/**
 *
 * HjortmusMage very date
 */
//
///Users/hjort/Code/codeit16/pong_sample/HjortmusMagePongPaddle.java
public class HjortmusMagePongPaddle extends AbstractPongPaddle {

    public PongMove onGameTick (PongGame game) {
        Rectangle paddle = game.getPaddle(this);
        double h;
        if (whichPaddle(game) == whichBallDirection(game)) {
            h = calcH(game);
        }
        else {
            h = calcOpposite(game);
        }
        double heightBall = game.getBall().getCenterY();
        double target = fold(h + heightBall, game);
        final double offset = game.getPaddle(this).getHeight()/2 - 1;
        final int direction = game.getBall().getDirection().getY() < 0 ? -1 : 1;
        int finalDirection = foldDirection(h + heightBall, direction, game);
        return moveToPos(target, game);
    }

    private int foldDirection(double d, int initialDir, PongGame game) {
        double gameHeight = game.getHeight();
        if (d >= 0 && d <= gameHeight) {
            return initialDir;
        }
        else if (d < 0) {
            return foldDirection(-d, -initialDir, game );
        }
        return foldDirection(2 * gameHeight - d, -initialDir, game);

    }

    private double fold(double d, PongGame game) {
        double gameHeight = game.getHeight();
        if (d >= 0 && d <= gameHeight) {
            return d;
        }
        else if (d < 0) {
            return fold(-d, game );
        }
        return fold(2*gameHeight - d, game);
    }

    private double distanceToEnd(PongGame game) {
        if (whichBallDirection(game) == -1) {
            double paddleMargin = game.getPaddle(this).getX() + game.getPaddle(this).getWidth();
            return game.getBall().getCenterX() - paddleMargin;
        }
        return game.getPaddle(this).getX() - game.getBall().getCenterX();
    }

    private PongMove moveToCenter(PongGame game) {
    	return moveToPos(game.getHeight()/2.0, game); 
    }

    private PongMove moveToPos(Double destinY, PongGame game) {
        Rectangle paddle = game.getPaddle(this);
    	double paddleLocation = paddle.getY()+ paddle.getHeight() / 2;
    	if(paddleLocation>=destinY){
    		return PongMove.DOWN;
    	} else {
    		return PongMove.UP;
    	}
    }

    private double calcH(PongGame game){
        double currX = Math.abs(game.getBall().getDirection().getX());
        double currY = game.getBall().getDirection().getY();
        return (distanceToEnd(game) * (currY/currX));
    }

    private double calcOpposite(PongGame game) {
        double currX = Math.abs(game.getBall().getDirection().getX());
        double currY = game.getBall().getDirection().getY();
        double paddleMargin;
        if (whichPaddle(game) == -1) {
            paddleMargin = game.getPaddle(this).getX();
            paddleMargin += game.getPaddle(this).getWidth();
        } else {
            paddleMargin = game.getWidth() - game.getPaddle(this).getX();
        }
        double fieldLength = game.getWidth() - 2*paddleMargin;
        return ( (distanceToEnd(game) + game.getWidth()) * (currY/currX));
    }

    private int whichBallDirection(PongGame game) {
        Vector2D ballDirection = game.getBall().getDirection();
        return ballDirection.getX() < 0 ? -1 : 1;
    }

    private int whichPaddle(PongGame game) {
        Rectangle paddle = game.getPaddle(this);
        if (paddle.getX() < 20) {
            return -1;
        }
        return 1;
    }
}
